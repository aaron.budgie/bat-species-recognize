%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:calcula el espectrograma con 1024 pts NFFT, opci�n 0 
%espectrograma ultrasonido %fs=192K opcion 1 espectrograma audible
%x=se�al
%tv:tama�o venta: ultrasonido:100; audible:50
%fs:192000ultrasonido; 44100: audible
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
function [T,F,img] = espectrograma (x,tv,fs,tipo,pintar)

if pintar
   figure
end
%con muestras
% B = SPECTROGRAM(x,hanning(tv),ceil(tv/2),1024,fs);
% % EB=(abs(B).^2)/tv;
% % [F,N]=size(B);
% % imagesc([1:N]*ceil(tv/2),[0:F-1]./F,10*log10(abs(B)));axis xy

%con ejes de tiempo(s) y frecuencia(Hz)
% [B,F,T] = SPECTROGRAM(x,hanning(tv),tv/2,1024,fs);
% imagesc(T,F,10*log10(abs(B)));axis xy
% % C=10*log10(abs(B));

%salida, imagen reflejada verticalmente
% s= size(B);
% lmatriz=floor(s(2));
% for i = 1: lmatriz
%     C(i,:)=B(:,lmatriz+1-i);
% end









if tipo ==0
%con ejes de tiempo(s) y frecuencia(Hz)
[B,F,T] = SPECTROGRAM(x,hanning(tv),tv/2,1024,fs);
if pintar
   imagesc(T,F,10*log10(abs(B)));axis xy
end
end

if tipo ==1
%con ejes de tiempo(s) y frecuencia(Hz)
[B,F,T] = SPECTROGRAM(x,hanning(tv),tv/2,1024,fs);
alfa=0.1;
Vm=max(max(abs(B)));
umbral=(alfa*Vm);
%umbral=Vm-mean(B);
ind=find(abs(B)<umbral);
B(ind)=zeros(size(ind));
if pintar
   imagesc(T,F,10*log10(abs(B)));axis xy
end
end
%salida para histograma
img=10*log10(abs(B));

