%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:modulo con cosceno, a la frecuencia de la parte baja de la
%llamada
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function ym = modular (x,Fs,fm)
w=2*pi*fm;
t=0:1/Fs:(1/Fs)*length(x)-(1/Fs);
c=cos(w*t);
ym=x.*c;
