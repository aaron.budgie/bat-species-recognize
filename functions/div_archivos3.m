%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:divide entre 6 los archivos de audio de entrada
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function div_archivos3 (grab_in)

   str=grab_in;
   [y,fs,nb]=wavread(str);
   
   l=floor(length(y)/6);
   y1=y(1:l);
   y2=y(l+1:2*l);
   y3=y(2*l+1:3*l);
   y4=y(3*l+1:4*l);
   y5=y(4*l+1:5*l);
   y6=y(5*l+1:6*l);   
   
   str1=strcat(str,'_','1.wav');
   str2=strcat(str,'_','2.wav');
   str3=strcat(str,'_','3.wav');
   str4=strcat(str,'_','4.wav');
   str5=strcat(str,'_','5.wav');
   str6=strcat(str,'_','6.wav');
   
    wavwrite(y1,fs,nb,str1);
    wavwrite(y2,fs,nb,str2);
    wavwrite(y3,fs,nb,str3);
    wavwrite(y4,fs,nb,str4);
    wavwrite(y5,fs,nb,str5);
    wavwrite(y6,fs,nb,str6);
