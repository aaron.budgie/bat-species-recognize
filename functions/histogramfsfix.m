%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:%calcula histograma y da el valor de frecuencia
% max�mo y m�nimo para filtrar
% para una fs fija de 44100
% es decir una bw de 0.2297
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [fmin,fmax]= histogramfsfix(x,Fs,tvo)
B = SPECTROGRAM(x,hanning(tvo),tvo/2,1024,Fs);
h=sum(B');
h=abs(h);
% figure
% plot(h)
% umbral al 80% del maximo
 umbral=0.8*max(h); %umbral sin denoise
%umbral=0.03*max(h); %umbral con denoise    
%me quedo con la parte que supera el umbral
I=find(h>umbral);

if length(I) < 2  % si no hay muestra (evita error)
   I=[2 4];%inventada solo para q no pare simulacion
end
% figure
% plot(h(I))
s=size(I);
f=I(1); %el first es el primer valor
l=I(s(2));  %el last es el ultimo
%el maximo de h corresponde con su maximo en frecuencia
s2=size(h);
fmin=f/s2(2);
fmax=l/s2(2);

bw=fmax-fmin;

% ajusto el ancho de banda en torno al centro
dif = 0.2297-bw;
fmax=fmax+dif/2;
fmin=fmin-dif/2;

% evita que pare la ejecucion en caso de error
if fmin < 0 | fmin > 1 |  fmax < 0 | fmax > 1
    fmin = 0.3;
    fmax = fmin + 0.2297;
end
%fuck yeah

