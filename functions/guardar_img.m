%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:guarda la imagen de la figura activa en ese momento
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function guardar_img(m_ultra, vl_ultra, m_aud, vl_aud, filename_u, filename_a)

fs=192000;
fsa=44100;
nb=16;
tvo=100;
tvd=400/8;

s=size(m_ultra);
for i = 1: s(1) %que recorra todas las llamadas
   
   str=num2str(i);
   str1=strcat(filename_u,str);
   str2=strcat(filename_a,str);
   %espectrogramas y guardar imagenes
   img=espectrograma(m_ultra(i,1:vl_ultra(i)),tvo,fs,0);
   title(['Espectrograma llamada sin tratar'])
   xlabel('Time');ylabel('Frequency');
   print('-djpeg90', str1);
   % map=colormap;
   % imwrite(img,map,'tupac.jpg');

   img=espectrograma(m_aud(i,1:vl_aud(i)),tvd,fsa,1);
   title(['Espectrograma llamada audible'])
   xlabel('Time');ylabel('Frequency');
   print('-djpeg90', str2);
%    print('-dbitmap',i,'jpg');
close all
end

   % guardar_img(sec_r, lsec_r, sec, lsec,'grab4-u-', 'grab-a-') %llamada