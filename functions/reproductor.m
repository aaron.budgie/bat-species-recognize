%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:almacena archivos audio en disco
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function reproductor(m_ultra, vl_ultra, m_aud, vl_aud, n_call, filename_u, filename_a)

fs=192000;
fsa=44100;
nb=16;
% tvo=100;
% tvd=400/8;

%guardo audios
s=size(m_ultra);
for i = 1: s(1)
    
str=num2str(i);
str1=strcat(filename_u,str);
str2=strcat(filename_a,str);

wavwrite(m_ultra(i,1:vl_ultra(i)),fs,nb,str1);


amax= max(m_aud(i,1:vl_aud(i))); %amplitud maxima sonido
fmul= 0.9/amax; %factor de multiplicacion para un maximo de 0.9 de amoplitud

m_aud(i,1:vl_aud(i))=m_aud(i,1:vl_aud(i))*fmul; %aumento amplitud se�al audible


wavwrite(m_aud(i,1:vl_aud(i)),fsa,nb,str2);
end

save str1, m_ultra;

% %escucho
% soundsc(m_aud(n_call,1:vl_aud(n_call)),fsa);
% pause(2);
% soundsc(m_aud(n_call,1:vl_aud(n_call)),fsa);
% 
% %espectrogramas
% img=espectrograma(m_ultra(n_call,1:vl_ultra(n_call)),tvo,fs,0);
% title(['Espectrograma llamada sin tratar'])
% xlabel('Time');ylabel('Frequency');
% 
% 
% img=espectrograma(m_aud(n_call,1:vl_aud(n_call)),tvd,fsa,1);
% title(['Espectrograma llamada audible'])
% xlabel('Time');ylabel('Frequency');
% reproductor(sec_r, lsec_r, sec, lsec, 1, 'p1_u', 'p1_a') %llamada

%se�al en tiempo
% figure
% plot(m_ultra(n_call,1:vl_ultra(n_call)))