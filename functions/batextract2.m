%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:lee archivos, extrae llamadas, las guarda y reproduce
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function batextract2(grab_in,filename_u,filename_a)

    
 for j=1:1:6%6   
     
    turno=num2str(j); 
    str2=strcat(grab_in,'_',turno);  %empieza por grabxminxx_1
   [Y,Fs,nb]=wavread(str2);


%las proceso

   mkdir (turno);
   cd (turno);  

   [sec,lsec, sec_r, lsec_r]=batplayd (Y,Fs,nb);
   

%guardo imagen

   guardar_img(sec_r, lsec_r, sec, lsec, filename_u, filename_a);
   
%guardo audio
   reproductor(sec_r, lsec_r, sec, lsec, 1, filename_u, filename_a);   

   cd ..

   
 end
   
   
   
   
