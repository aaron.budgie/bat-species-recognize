%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:Adaptaci�n del audio
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [sec,lsec, sec_r, lsec_r]=batplayd (Y,Fs,nb)

%filtro paso alto y calculo las frecuencias max y min
%para filtrar la llamada

tvo=100; %tama�o ventana para 192K

[b,a] = BUTTER(10,0.3,'high');
Yf = filtfilt(b,a,Y);
%espectrograma(Yf,tvo,Fs)
% title(['Espectrograma sin ruido baja frecuencia'])
% xlabel('Time');ylabel('Frequency');

%reduzco ruido
[yTSNR,yHRNR]=WienerNoiseReduction(Yf,Fs,2000);

%saco las secuencias de llamadas
[speechactive, salida, vl, sec_r, lsec_r] = vad1 (yTSNR,Y);
n_calls=size(salida);
n_calls=n_calls(1); % numero de llamadas de la grabaci�n

for i = 1 : n_calls

    y = salida (i, 1:vl(i));
[fmin,fmax]=histogramfsfix(y,Fs,tvo);

%filtro paso banda
[b,a] = BUTTER(10,[fmin fmax]);
y2 = filtfilt(b,a,y);
%espectrograma(y2,tvo,Fs)
% title(['Espectrograma llamada filtrada'])
% xlabel('Time');ylabel('Frequency');

%modulo
fm=fmin*Fs/2;
y3=modular(y2,Fs,fm);
%espectrograma(y3,tvo,Fs)
% title(['Espectrograma llamada en baja frecuencia'])
% xlabel('Time');ylabel('Frequency');


%filtro antialiasine
bw=fmax-fmin; %ancho de banda normalizado
factd=ceil(1/bw);
tvd=tvo/factd; %nueva ventana
[b,a] = butter(10,0.5); % se suma el ruido por igual, no fmin para no distorsionar
y4 = filtfilt(b,a,y3);
%espectrograma(y4,tvo,Fs)


%diezmo para ocupar todo el espectro 
%y5=decimate(y4,factd);
y5=resample(y4,1000,floor(1000/bw));
%espectrograma(y5,tvd,Fs/factd)
% title(['Espectrograma llamada a Fs audible'])
% xlabel('Time');ylabel('Frequency');

%escucho la llamada
% soundsc(y5,Fs/factd)
% Frecuencia_maxima = Fs/2/factd

%las guardo en la siguiente columna en la matriz de salida
%guerdando su longitud
l=length(y5);
lsec(i)=l;
sec(i,1:l)=y5;

end

%guardo el resultado y escucho
% WAVWRITE(y5,Fs/factd,nb,'salida')
% [x,fs]=wavread('salida')
% soundsc(x,fs);
