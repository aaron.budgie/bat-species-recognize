%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Desconocido, basado en trabajo de Sohn
%Fecha:2006.10.09
%Descripcion:"A voice activity detector employing soft decision based noise
%spectrum adaptation"
%%%Modificaci�n
%Autor:Aaron Henriquez
%Fecha:09/2012
%Descripcion:Adaptar codigo al audio de los murcielagos:
%No cortar las locuciones
%Dos clases de umbrales
%Cambio de estado (de actividad/no actividad) no inmediato
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [speechactive, salida, vl, sec_r, lsec_r] = vad1 (signal, s_ruidosa)

%divide se�al en 128 (longitud inicial alrededor de 20000)
L=128;%128;                                          
N=floor(length(signal)./L);                     
input_signal=signal(1:L.*N);                     
input_signal_r=s_ruidosa(1:L.*N);

%????????fft???????lambda???? coge 128 muestras por trama
firstframe = input_signal(1:L);
fftfirstframe = fft(firstframe);
lambda = ones(L,1).*(abs(fftfirstframe).^2);
lambda0 = ones(L,1).*((100./(2.^15)).^2);      %??????????snr??????
beta = lambda+lambda0;                         
%=================================
% snrbuffer = zeros(10,1);
snrthr=5;
% snrtempcnt=0;
%=================================

snr = zeros(N,1);
snrtemp=zeros(N,1);
proratio = 10;                                 %?????????probabilityratio????10
contador=0; %para que vaya por secuencias
k=0; %indice fila matriz de salida conlas llamadas
ini=1/L; %por si la grab comienza con se�al

for i = 1 : N
    signalframe = input_signal((i-1).*L+1 : i.*L);  %ventana actual
    framesigfft = fft(signalframe);                 %espectro ventana actual
    snr = (1./L).*sum(abs(framesigfft).^2./beta);
    snrtemp(i) = snr;
    contador = contador +1;
    guardar =0; % se pone a uno cuando acaba una locucion
    if i <= 10
        speechactive(i) = 1;
    else
        if speechactive(i-1)==1
            if contador < 150%100    % si no ha pasado mucho tiempo desde la ultima llamada (0.0667s=100/192000*128)
                speechactive(i) = 1;
            elseif snrtemp(i) > snrthr
                speechactive(i) = 1;
            else
                logi = (snrtemp(i-19 : i-1) > snrthr);
                if sum(logi) >= 3
                    speechactive(i) =1;
                else
                    speechactive(i) = 0;
                    contador=0;
                end
            end
        else
            if (snrtemp(i) > snrthr && snrtemp(i-1) > snrthr)
                speechactive(i) = 1;
            else
                speechactive(i) = 0;
                contador=0;
            end
        end
    end
    
    if speechactive(i) == 0
        lambda =(1./(1+snr.*proratio)).*abs(framesigfft).^2+(snr.*proratio/(1+snr.*proratio)).*lambda;
        beta = lambda + lambda0;
        
        %=================================
%         if snrtemp(i) <=10
%             snrtempthr=snrtemp(i);
%         end
%         for j=1:19
%             snrbuffer(j+1) = snrbuffer(j);
%             snrbuffer(j)=snrtempthr;
%         end
%         snrmax=max(snrbuffer(1:20));
%         snrtempcnt=snrtempcnt+1;
%         if mod(snrtempcnt,20)==0
%             if snrmax>=5
%                 snrthr=snrmax;
%             end
%         end 
        %=================================
    end
    if i > 1
       if speechactive(i-1)==0 & speechactive(i)==1
           ini=i-10; %guardo donde empieza la locucion(algo antes)
       end  
       if speechactive(i-1)==1 & speechactive(i)==0
           fin=i; %guardo donde empieza la locucion
           guardar=1;
           k=k+1;
       end 
    end
    
    if guardar == 1
        longitud = (fin-ini)*L; %longitud en muestras de la llamada a buscar
        salida(k,(1:longitud+1))= input_signal(ini*L:fin*L);
        vl(k)=longitud;
        
        sec_r(k,(1:longitud+1))= input_signal_r(ini*L:fin*L);
        lsec_r(k)=longitud;
%         size(salida(k,(1:longitud+1)))
%         size(input_signal(ini*L:fin*L))
    end
    
end

%==========================
figure;
plot([1:1:L.*N],input_signal);
hold on
plot([1:L:L.*N],speechactive.*0.2,'r');
%========================== ...