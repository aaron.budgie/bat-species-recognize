%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripción:resumen estadistico tras 200 iteraciones
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num_clases=7;

for clase=1:num_clases
    
for it = 1:200
    
   tabla_resultados=cell2mat(results_m(it));
   
   falso_rechazo=0;
   falso_positivo=0;
   
for i=1:num_clases
    for j=1:num_clases
        if j== clase
           if i==j
               acierto = tabla_resultados(i,j); %un unico valor acierto
               
           else
               
               falso_rechazo=falso_rechazo+tabla_resultados(i,j);
           end
        else
%            if i~=j
%                acierto2 = acierto2+tabla_resultados(i,j); %un unico valor acierto
%                
%            else
               if i== clase
                  falso_positivo =falso_positivo+tabla_resultados(i,j);
               end
%            end            
           
        end
    end
end
   FP(it)=falso_positivo;
   FN(it)=falso_rechazo;
   VP(it)=acierto;
   VN(it)=100-falso_positivo;
   
end

vsensivity=VP./(VP+FN);
vspecificity=VN./(VN+FP);
vPPP=VP./(VP+FP);
vNPP=VN./(VN+FN);

vaccuracy=(VP+VN)./(VP+FN+VN+FN);
vFPR=1-vspecificity;
vFDR=FP./(FP+VP);

sensivity.value=mean(vsensivity);
specificity.value=mean(vspecificity);
PPP.value=mean(vPPP);
NPP.value=mean(vNPP);

accuracy.value=mean(vaccuracy);
FPR.value=mean(vFPR);
FDR.value=mean(vFDR);

sensivity.desv=std(vsensivity);
specificity.desv=std(vspecificity);
PPP.desv=std(vPPP);
NPP.desv=std(vNPP);

accuracy.desv=std(vaccuracy);
FPR.desv=std(vFPR);
FDR.desv=std(vFDR);

M{clase}=[sensivity.value,sensivity.desv,specificity.value,specificity.desv,PPP.value,PPP.desv,NPP.value,NPP.desv,accuracy.value,accuracy.desv,FPR.value,FPR.desv,FDR.value,FDR.desv];


end

for k=1:num_clases
    vec=cell2mat(M(k));
    goal(k)=vec(14);
end

sum(goal)/num_clases

    
    
