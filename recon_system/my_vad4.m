%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:%detector de voz en tiempo, es valido porque la se�al tiene poco ruido
%lo modifico para que solo detecte el primer golpe de audio y ignore 
%el segundo, por tanto es mas sencillo
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [out,longitud,IPI] = my_vad4 (in,pintar,class)

N=length(in);
contador=0; %para que no se ponga a cero desde la primera muestra q no pase umbral
contador2=0;  %para que no se ponga a uno desde la primera muestra q pase el umbral
umbral=0.05; %%0.05 valor de amplitud de la se�al. a�ado experimentalmente
umbral2=0.1;

fin =0; % cuando coge una locucion acaba y deja de buscar
part_no=0;   %numero de partes detectadas
IPI=floor(N/2);

for i = 1:N
        
     guardar =0; % se pone a uno cuando acaba una locucion
    if i <= 10
        speechactive(i) = 0;
    else
        if speechactive(i-1)==1
                if abs(in(i)) > umbral2
                    speechactive(i) =1;
                else
                    contador=contador+1;
                    speechactive(i) =1;
                end
                if contador > 100 %250
                    speechactive(i) =0;
                    contador=0;
                end
        else
                if abs(in(i)) < umbral
                    speechactive(i) =0;
                else
                    contador2=contador2+1;
                    speechactive(i) =0;
                    if contador2 > 50 %1500
                       speechactive(i) = 1;
                       contador2=0;
                    end
                end
        end
    end
%evita error en caso de que llegue al final sin detectar nada    
if i==N
    guardar=1;
    ini=1;
end
    if i > 1
       if speechactive(i-1)==0 & speechactive(i)==1
           ini=i-80;%70 %guardo donde empieza la locucion(algo antes)
           if ini < 1
               ini=1;
           end
       end  
       if speechactive(i-1)==1 & speechactive(i)==0
           fin=i;%20 %guardo donde empieza la locucion
           guardar=1;
       end 
    end
    
    if guardar == 1
        if ini < 1
            ini=1;
        end
        if fin > N
            fin=N;
        end
        
        if part_no ==0
           longitud = fin-ini; %longitud en muestras de la llamada a buscar       
           out(1:longitud+1)=in(ini:fin); %tramo de se�al se 
           
           ini_part0=ini;
           fin_part0=fin;
        elseif part_no ==1
            IPI=ini-fin_part0;
        end
        part_no=part_no+1;
    end
   
end
%se�al que recojo no coincide con speech_active sino es un poco antes 

    select_signal=[zeros(1,ini_part0) ones(1,fin_part0-ini_part0) zeros(1,N-fin_part0)];
% ini
% fin
% length(in)-fin
IPI=IPI/300; %intervalo entre pulsos
longitud=longitud/35;% param duracion
% sig_detected= [zeros(1,ini) ones(1,fin-ini) zeros(1,length(in)-fin)];
%==========================
if pintar
   figure;
   plot(in);
   hold on
   a=plot(select_signal.*0.2,'r','LineWidth',2);
   hold on
   b=plot(speechactive.*0.3,'g','LineWidth',2);
   
   stclase=num2str(class);
   msg = strcat('detector de locuciones');
   msg2 = strcat('vad',stclase);
   title([msg])
   xlabel('Tiempo');ylabel('Amplitud');
   legend([a,b],'duracion','actividad');
   str = strcat('C:\Users\Aar�n\Documents\MATLAB\figurasmemoria\vad\',msg2);
%    print('-djpeg90', str);
end
%==========================