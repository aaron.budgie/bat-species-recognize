%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:crea la grafica ROC inversa a partir resultados de FP y FR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
% clase=13;

for clase= 1:7
stclase=num2str(clase);
fp1=cell2mat(FPm(clase));
fn1=cell2mat(FNm(clase));

%oscilaciones por pocas medidas
for i = 1:length(fp1)-1;
    if fp1(i)<fp1(i+1);
       fp1(i)=fp1(i+1);
       
    end
    if fn1(i) > fn1(i+1);
       fn1(i) = fn1(i+1);
    end
end

[b,a] = BUTTER(2,0.1);  %0.3
fpf1 = filtfilt(b,a,fp1);
fnf1 = filtfilt(b,a,fn1);


%rectificacion
for i = 1:length(fp1);
  if fnf1(i)>100
      fnf1(i)=100;
  end
  if fnf1(i)< 0
      fnf1(i)=0;
  end

  if fpf1(i)< 0
      fpf1(i)=0;
  end
end
col=hsv(7);
acierto=100-fpf1;

plot(fnf1,acierto,'color',col(clase,:),'LineWidth',2);
hold on

%calculo area
% AUC(clase)=(polyarea(fnf1/100,acierto/100));
% ar(clase)=100-(100*ar(clase));

% AUC(clase)=trapz(acierto,fnf1)/100;
% AUC(clase)=100-AUC(clase);
% AT=1e5;

% AUC(clase) = ppval(fnint(csape(fnf1,acierto)),max(fnf1)); 
% AUC(clase) =AreaUnderCurve(fnf1,acierto)/100;
end

% 100-(100*AUC/AT) %porcentaje de AUC
% 100-AUC

% msg = strcat('ROC inv',stclase);
msg = 'ROC NEG';
title([msg],'FontName','Arial','FontSize', 14)
xlabel('FN','FontName','Arial','FontSize', 14);ylabel('Verification negative','FontName','Arial','FontSize', 14);
legend('class1','class2','class3','class4','class5','class6','class7','FontName','Arial','FontSize', 14);
% 
% xlabel('FR');ylabel('Negative Verification');
% legend('class1','class2');

% %grafica ROC
% msg = strcat('ROC_inv',stclase);
% str = strcat('C:\Users\Aar�n\Documents\MATLAB\results7\ROC\',msg);
% print('-djpeg90', str);

