%calcula histograma (suma filas de un espectrograma y los valores de 
%frecuencia central,maxima y minima para el vector de caracteristicas

function [fmin,fmax]= acotar_call(x,Fs,tvo,pintar)



%%% Paso 1 acotar la llamada en frecuencia

[B,F,T] = SPECTROGRAM(x,hanning(tvo),tvo/2,1024,Fs);%puntos 1024,1024*2
if pintar==1
%    imagesc(abs(B));axis xy
   imagesc(T,F,abs(B));axis xy
   eje=axis;
end

sB=size(B);

ini_sinruido=sB(1)*0.3;

aB=abs(B(ini_sinruido:sB(1),:)); %quito el rudio paso bajo para que no moleste

% figure;
% imagesc(abs(aB));axis xy

h=sum(aB');
% umbral al 80% del maximo
 umbral=0.6*max(h); %umbral sin denoise   0.8
%me quedo con la parte que supera el umbral
I=find(h>umbral);

% if length(I) < 2  % si no hay muestra (evita error)
%    I=[20 54];%inventada solo para q no pare simulacion
% end
% figure
% plot(h(I))
s=size(I);
f=I(1); %el first es el primer valor
l=I(s(2));  %el last es el ultimo



%el maximo de h corresponde con su maximo en frecuencia
s2=size(h);
fmin=(f+ini_sinruido)/sB(1); %entre 0 y 1 siendo 1 96Khz
fmax=(l+ini_sinruido)/sB(1);

aBs=aB(f:l,:);  %solo cojo las filas pertenecientes a la llamada
% size(Bs)

if pintar ==1
   figure;
%    imagesc(aBs);axis xy
   imagesc(T,F,aBs);axis xy
%    axis(eje)
end
% close all
end