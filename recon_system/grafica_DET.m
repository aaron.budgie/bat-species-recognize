%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:crea la grafica DET a partir resultados de FP y FR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
% clase=13;  %para una sola
num_clases=7;

for clase= 1:num_clases  %para todas:13
stclase=num2str(clase);
fp1=cell2mat(FPm(clase));
fn1=cell2mat(FNm(clase));

%oscilaciones por pocas medidas
for i = 1:length(fp1)-1;
    if fp1(i)<fp1(i+1);
       fp1(i)=fp1(i+1);
       
    end
    if fn1(i) > fn1(i+1);
       fn1(i) = fn1(i+1);
    end
end

[b,a] = BUTTER(2,0.1);  %0.3
fpf1 = filtfilt(b,a,fp1);
fnf1 = filtfilt(b,a,fn1);


%rectificacion
for i = 1:length(fp1);
  if fnf1(i)>100
      fnf1(i)=100;
  end
  if fnf1(i)< 0
      fnf1(i)=0;
  end

  if fpf1(i)< 0
      fpf1(i)=0;
  end
end

% colores='rgbcmykrgbcmy';
col=hsv(7);


%cojo la parte de la curva que me interesa
% sfpfn = fpf1+fnf1;
% pos_min=find(sfpfn==(min(sfpfn)));

% plot(fpf1(pos_min-8:pos_min+8),fnf1(pos_min-8:pos_min+8),colores(clase))


plot(fpf1,fnf1,'color',col(clase,:),'LineWidth',2);
hold on

%calculo area
ar(clase)=(polyarea(fnf1/100,fpf1/100))*100;
% 
AUC(clase)=trapz(fnf1,fpf1)/100;
% AT=1e5;
% % AUC(clase) =AreaUnderCurve(fpf1,fnf1)/100;

end
% 100*AUC/AT %porcentaje de AUC
AUC
ar

% msg = strcat('DET',stclase);

msg = 'DET';
title([msg],'FontName','Arial','FontSize', 14);
xlabel('FP','FontName','Arial','FontSize', 14);ylabel('FN','FontName','Arial','FontSize', 14);
legend('class1','class2','class3','class4','class5','class6','class7','FontName','Arial','FontSize', 14);

% xlabel('FA');ylabel('FR');
% legend('class1','class2');
% %grafica DET
% msg = strcat('DETbmp',stclase);
% str = strcat('C:\Users\Aar�n\Documents\MATLAB\results7\DET\',msg);
% print('-djpeg90', str);
% print('-dbitmap', str);
