%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%Reconocimiento automatico multimoledo train & test
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [error,tabla_errores] = id_gmm_umbral_last_sel_fixed(no_gaus,umbral) 

close all
% Parametros generales
Ntr = 30;   %n� muestras train
Nts = 20; %n� muestras de test

no_files = 50; %numero ficheros base de datos

% n_tr = 1;  % numero de pista de un locutor a utilizar en entrenamiento (1 o 2)
% n_ts = 1;  % numero de pista de un locutor a utilizar en test (1 o 2) 
dir1 = 'C:\Users\Aar�n\Documents\copia tipos de llamadas\tipos de llamadas\';
num_clases =7;
tabla_resultados= zeros(num_clases,num_clases);
suma_diagonal=0;
no_col=1; %numero de columnas de los parametros extra a�adidos al vector
no_row=3;
% no_par_extra=4; %no parametros extra a�adidos al vector

file_record = [];

% Parametros para el algoritmo EM
M = no_gaus;
tmax = 200; % numero maximo de iteraciones 
init = 'cmeans'; % metodo para encontrar el modelo inicial


% Inicializacion de variables
fs = 0;
i = 0;
j = 0;
distmin = 0;
log_l = -inf;

log_lp= -inf;
l = 0;


acierto =0;
acierto2 =0;
falso_rechazo=0;
falso_positivo =0;
tabla_indv=[];


file = [];
msg = [];
s = [];
v = [];
model = [];
mu = [];
sigm = [];
c = [];
code = cell(1,Ntr);
aux_logl = [];
logl_matrix = [];
y = [];
ym = [];


%% Fase de entrenamiento
% con las muestras de entrenamiento creo un modelo para cada clase
for n = 1 : num_clases
    clase=num2str(n);
    if n<10
        clase=strcat('0',clase);
    end
 dir=strcat(dir1,'clase',clase,'\');
 
for i = 1:Ntr
    
    num= file_select (no_files);
%     num=num2str(i);

% este bucle es para que no coja dos veces el mismo archivo
    for it=1:length(file_record)
        if num == file_record(it)
            num= file_select (no_files);
            it=1;
        end
    end
   file_record(i)=str2num(num);
   
 % los ficheros menosres que 20 son 01,02,etc...  
    if file_record(i)<10
        num=strcat('0',num);
    end
   
    file = strcat(dir,'muestra_audible',num,'.wav');
   % file = strcat(dir1,'muestra_original',num,'.wav');
   
    msg = sprintf('Reading training file #%d: %s',i,file); disp(msg);

    [s,fs] = wavread(file);
    
%     disp('Running noise filter') %% quito el ruido para que el detector de golpes de audio lo haga bien
%     [s,yHRNR]=WienerNoiseReduction(s,fs,500);
%     plot(s)
%     title('tras quitar ruido');   
    

%llevo llamada al centro

%    if i < 3
%      s = modular2center(s',fs,1);
%      input('Press ENTER to continue');
%    else
%      close all
%      s = modular2center(s',fs,0);
%    end



%     disp('Running Voice Activity Detector')    %% se queda solo con la
%     llamada, pero da peor resultado

    [s,dur,IPI]=my_vad4(s,0,clase);
    
%     length(s)
%     dur
    
%     if i <2
%         figure
%         plot(s)
%     end
%   s = dav(s',fs); %este no vale no detecta nada
%     [s,dur]=vad1(s,0);
% if i < 3
%     [s,dur]=vad1(s,0);
% end
%    if i < 3
% % %     B = SPECTROGRAM(s,hanning(26),26/2,512,fs);
% % %     figure;
% % %     imagesc(abs(B));axis xy 
%     [s,dur,IPI]=my_vad4(s,1);
%     input('Press ENTER to continue');
%    else
%      close all
%      [s,dur,IPI]=my_vad4(s,0);
%    end

    disp('Extracting training features');
    
    [fc,fmin,fmax,fini,fend,fmid,bw,st1,st2]=fc_ex_pruebasdouble(clase,num,dur);
    slope = calc_slope(dur,fini,fend);
    
%  if i<8
% %    pitch_m(n,i)=pitch;
%    bw_m(n,i)=bw;
% 
% end
    deltaf=fend+fini;
    deltaf=repmat(deltaf,no_row,no_col);
   
%a�ado fc al vector de caracteristicas
 
    fc=repmat(fc,no_row,no_col);
    fmin=repmat(fmin,no_row,no_col);
    fmax=repmat(fmax,no_row,no_col);
    fini=repmat(fini,no_row,no_col);
    fend=repmat(fend,no_row,no_col);
    fmid=repmat(fmid,no_row,no_col);
    bw=repmat(bw,no_row,no_col);
    dur=repmat(dur,no_row,no_col);
    
    slope=repmat(slope,no_row,no_col);
    IPI=repmat(IPI,no_row,no_col);
    
    media1=repmat(st1.media,no_row,no_col);
    varianza1=repmat(st1.varianza,no_row,no_col);
    skew1=repmat(st1.skew,no_row,no_col);
    kurt1=repmat(st1.kurt,no_row,no_col);
    tupac1=repmat(st1.tupac,no_row,no_col);
    
    media2=repmat(st2.media,no_row,no_col);
    varianza2=repmat(st2.varianza,no_row,no_col);
    skew2=repmat(st2.skew,no_row,no_col);
    kurt2=repmat(st2.kurt,no_row,no_col);
    tupac2=repmat(st2.tupac,no_row,no_col);
%     tupac=tupac(1:no_row);
%     size(fc)
%     size(fmin)
%     size(fmax)
%     size(fini)
%     size(fend)
%     size(dur)
%     size(IPI)
%     size(slope)
%     size(media1)
%     size(varianza1)
%     size(media2)
%     size(varianza2)
    %frec=[fc fmin fmax fini fend dur IPI slope media varianza skew kurt tupac];
%     frec=[fc fmin fmax fini fend dur IPI slope deltaf fmid media1 varianza1 skew1 kurt1 tupac1 media2 varianza2 skew2 kurt2 tupac2 bw];
   frec=[fc fmin fmax fini fend dur IPI slope media1 varianza1 kurt1 fmid media2 varianza2 deltaf];
   
    v=[frec];   %de esta manera no a�ado los parametros de frecuencia a cada vector
    %sino que se crea un vector nuevo de igual valor con cada parametro
    

%     disp('Training model (this step may take a while)');

    
% %     %for debug
%    if i < 5
%       figure;
%       plot(v')
%       input('Press ENTER to continue');
%    else
%      close all
%    end
    
    
%un modelo por cada fichero y luego saco la media

   [mu_Sneg,sigm_Sneg,c_Sneg] = gaussmix3(v',[],[],M,'kp');
   
   
%    meanp_v{n}=meanp; %se queda con la matriz de medias del ultimo prestd de cada clase(mejorar)
   
   code{i} = struct('mu',mu_Sneg','sigm',sigm_Sneg','w',c_Sneg);
%    code = struct('mu',mu_Sneg','sigm',sigm_Sneg','w',c_Sneg);  % un code por clase no la locura de antes
   
   
end

   file_record=[];% vuelo a poder coger cualquier archivo para la siguiente clase

   m_code{n}=code; %matriz de matrices de structuras, una matriz de cada clase contiene N matrices de estructuras
end

dir2 = 'C:\Users\Aar�n\Documents\copia tipos de llamadas\tipos de llamadas\';

% bw_m 
for n = 1 : num_clases
    clase=num2str(n);
    if n<10
        clase=strcat('0',clase);
    end
 dir=strcat(dir2,'clase',clase,'\');
 
 
% Fase de test
for j = 1:Nts
    num= file_select (no_files);

% este bucle es para que no coja dos veces el mismo archivo
    for it=1:length(file_record)
        if num == file_record(it)
            num= file_select (no_files);
            it=1;
        end
    end
   file_record(j)=str2num(num);
 
    % los ficheros menosres que 20 son 01,02,etc...  
    if file_record(j)<10
        num=strcat('0',num);
    end
    
    file = strcat(dir,'muestra_audible',num,'.wav'); 
    msg = sprintf('Reading testing file #%d: %s',j,file); disp(msg);
    [s,fs] = wavread(file);
    
    
    
%     % modulo al centro
%     s = modular2center(s',fs,0);




%     disp('Running Voice Activity Detector')
    [s,dur,IPI]=my_vad4(s,0,clase);
% %    s = dav(s',fs);
%     s=vad1(s);

%    if j < 2
%     s=my_vad3(s,1);
%    else
%     s=my_vad3(s,0);
%   end   

    disp('Extracting testing features');
    
    [fc,fmin,fmax,fini,fend,fmid,bw,st1,st2]=fc_ex_pruebasdouble(clase,num,dur);
    slope = calc_slope(dur,fini,fend);

    deltaf=fend+fini;
    deltaf=repmat(deltaf,no_row,no_col);
%a�ado fc al vector de caracteristicas
   
    fc=repmat(fc,no_row,no_col);
    fmin=repmat(fmin,no_row,no_col);
    fmax=repmat(fmax,no_row,no_col);
    fini=repmat(fini,no_row,no_col);
    fend=repmat(fend,no_row,no_col);
    fmid=repmat(fmid,no_row,no_col);
    bw=repmat(bw,no_row,no_col);
    dur=repmat(dur,no_row,no_col);
    
    slope=repmat(slope,no_row,no_col);
    IPI=repmat(IPI,no_row,no_col);
    
    media1=repmat(st1.media,no_row,no_col);
    varianza1=repmat(st1.varianza,no_row,no_col);
    skew1=repmat(st1.skew,no_row,no_col);
    kurt1=repmat(st1.kurt,no_row,no_col);
    tupac1=repmat(st1.tupac,no_row,no_col);
    
    media2=repmat(st2.media,no_row,no_col);
    varianza2=repmat(st2.varianza,no_row,no_col);
    skew2=repmat(st2.skew,no_row,no_col);
    kurt2=repmat(st2.kurt,no_row,no_col);
    tupac2=repmat(st2.tupac,no_row,no_col);
    
    
%     tupac=tupac(1:no_row);
%frec=[fc fmin fmax fini fend dur IPI slope media varianza skew kurt tupac];
%     frec=[fc fmin fmax fini fend fmid dur IPI slope media1 varianza1 skew1 kurt1 tupac1 media2 varianza2 skew2 kurt2 tupac2];
    frec=[fc fmin fmax fini fend dur IPI slope media1 varianza1 kurt1 fmid media2 varianza2 deltaf];
    
    v_T=[frec];
    

%     disp('comparing, wait please');


%este bucle es para si m_code tiene un modelo para cada i    
    for t = 1: length (m_code)   %t son clases
       model_class= cell2mat(m_code{t});
       aux_logl = [];
       
       for k = 1:length(code)   % k es el numero de modelos de cada clase que coincide con el numero de train
           model= model_class(k);
           
%            size(model.mu)
%            size(model.sigm)
%            size(model.w)
           
           [y,ym] = lmultigauss(v_T, model);
           aux_logl(k) = mean(ym); 
         
%            aux_logl2(k) =10*log(abs(mean(mean(y))));

%            dirsave=strcat('C:\Users\Aar�n\Documents\MATLAB\calcEER\',clase,num);
%            save  'C:\Users\Aar�n\Documents\MATLAB\calcEER\' aux_logl
           
       end 
%                EER_m(t,n)=max(aux_logl);  % t es cada clase y k es cada muestra train, el maximo es el q utilizo en la desicion
               
%                max(aux_logl)

%                aux_logl



          if max(aux_logl) > log_l 
              
             % por si no da ninguno
             if max(aux_logl) > log_lp
                lp=t;
                log_lp = max(aux_logl);
             end
             
             if max(aux_logl) > umbral(t)  %si no supera el umbral sigue
%                  if mean(aux_logl2) < log_l2
                     
%                     log_l2 = mean(aux_logl2);   % gana el que tiene el y menor 
%                     log_l = max(aux_logl);   % gana el que tiene el ym mayor
                    
%                  if t==clase_en_test 
%                     if log_l > umbral %si es la clase compruebo el umbral                       
% %                        log_l = max(aux_logl);   % gana el que tiene el ym mayor 
%                        l = t;
%                        break;  %si supera el umbral ya paro, para los NO falsos positivos
%                     end
%                  else
                     log_l = max(aux_logl);   % gana el que tiene el ym mayor 
                     l = t;
%                  end  
                    
             end            
          end   
             
    end

%reseteo el valor para las muestras siguiente clase
log_l =-inf;
log_lp=-inf;
     
     %tabla de acierto: entradas por la izquierda cada columna es una clase de salida
     if l > 0
        tabla_resultados(l,n)=tabla_resultados(l,n)+1; 
     else
         tabla_resultados(lp,n)=tabla_resultados(lp,n)+1;
     end
end

    file_record=0;% vuelo a poder coger cualquier archivo para la siguiente clase

end

%muestro tabla resultados
tabla_resultados;
%porcentaje acierto global
num_test=Nts;
for i=1:num_clases
    for j=1:num_clases
        if i==j
            suma_diagonal = suma_diagonal+tabla_resultados(i,j);
        end
    end
end
acierto_global = suma_diagonal*100/num_test/num_clases; 

% disp(logl_matrix);   
error=100-acierto_global;
%tabla de errores
tabla_errores = calc_tablas (tabla_resultados,num_test,num_clases);


% save  'C:\Users\Aar�n\Documents\MATLAB\calcEER\EER_m' EER_m
end
