%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:crea la grafica EER a partir resultados de FP y FR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clase=7;

% for clase= 1:13
stclase=num2str(clase);
ejex=[-4:0.1:1.5];
fp1=cell2mat(FPm(clase));
fn1=cell2mat(FNm(clase));

% fp2=cell2mat(pFPm(clase));
% fn2=cell2mat(pFNm(clase));
% 
% fp1=[fp2 fp1];
% fn1=[fn2 fn1];
% 
% FPmx{clase} = fp1;
% FNmx{clase} = fn1;
% end
% save 'C:\Users\Aar�n\Documents\MATLAB\results5\EERf' FPmx FNmx

%oscilaciones por pocas medidas
for i = 1:length(ejex)-1;
    if fp1(i)<fp1(i+1);
       fp1(i)=fp1(i+1);
       
    end
    if fn1(i) > fn1(i+1);
       fn1(i) = fn1(i+1);
    end
end

[b,a] = BUTTER(2,0.1);  %0.3
fpf1 = filtfilt(b,a,fp1);
fnf1 = filtfilt(b,a,fn1);


%rectificacion
for i = 1:length(ejex);
  if fnf1(i)>100
      fnf1(i)=100;
  end
  if fnf1(i)< 0
      fnf1(i)=0;
  end

  if fpf1(i)< 0
      fpf1(i)=0;
  end
end

% col=hsv(13);

% plot(ejex,fpf1,'color',col(clase,:))
% hold on
% plot(ejex,fnf1,'color',col(clase,:))
% hold on
% end

% msg='EER ALL';
% title([msg])
% xlabel('Umbral');ylabel('FP/FN');
% legend('FP','FN');
% grid;
% legend('class1','class2','class3','class4','class5','class6','class7','class8','class9','class10','class11','class12','class13');

% plot(ejex,fp1)
% hold on
% plot(ejex,fn1,'r')
msg = strcat('EER',stclase);
% title([msg])
% xlabel('Umbral');ylabel('FP/FN');
% legend('FP','FN');
% 
figure

plot(ejex,fpf1,'LineWidth',2')
hold on
plot(ejex,fnf1,'r','LineWidth',2')
title([msg],'FontName','Arial','FontSize', 14)
xlabel('Umbral','FontName','Arial','FontSize', 14);ylabel('FP vs FN','FontName','Arial','FontSize', 14);
legend('FP','FN','FontName','Arial','FontSize', 14);
grid;

% %grafica EER
msg = strcat('EER_',stclase);
str = strcat('C:\Users\Aar�n\Documents\MATLAB\results7\EER\',msg);
print('-djpeg90', str);

