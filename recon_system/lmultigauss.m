%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:-
%Fecha:08/2013
%%Descripción:Verificacion GMM. Genera los scores
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [YM,Y] = lmultigauss(x,model)
% LMULTIGAUSS computes multigaussian log-likelihood
%
% Synopsis:
%
% [lYM,lY] = lmultigauss(x,model)
% 
% Inputs:
% x     : data [dim num_data] 
% model : struct with model parameters
%   .sigma : variances vectors [dim M] (diagonal of the covariance matrix)
%   .mu    : means vectors  [dim M]
%   .w     : weights [M 1]
%
% Outputs:
% YM: log-likelihoods [num_data M] (one mixture per column)
% Y:  lsum(YM)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
DEBUG = 0;
DEBUG1 = 0;

mu = model.mu;
sigma = model.sigm;
c = model.w;

% Const
[L,T] = size(x);
% M = size(c,1);
[N,M] = size(mu);
% dis =N-L;

if DEBUG 
    [size(x), size(mu), size(sigma), size(c)],
end

%chapuza para que no de error le quito columnas a sigma y mu o a x segun
%corresponda

if L < N
    sigma = sigma (1:L,:);
    mu = mu (1:L,:);
elseif N < L
    x = x (1:N,:);
end


% size(x)
% size(sigma)
% size(mu)
% disp('despues, ta chan!');

% repeating, changing dimensions:
X = permute(repmat(x',[1,1,M]),[1,3,2]);           % (T,L) -> (T,M,L) one per mixture
Sigma = permute(repmat(sigma,[1,1,T]),[3,2,1]);    % (L,M) -> (T,M,L)
Mu = permute(repmat(mu,[1,1,T]),[3,2,1]);          % (L,M) -> (T,M,L)

% %para testeo
% size(X)
% size(Sigma)
% size(Mu)

if DEBUG 
    size(X), size(Sigma), size(Mu), 
end

% -1/2*[(x-mui)'*(Ei)^-1*(x-mui)] (ver articulo Reynolds-Rose)
% Y = squeeze(exp( 0.5.*dot(X-Mu,(X-Mu)./Sigma)));    % L dissapears: (L,T,M) -> (T,M) 

lY = -0.5.*dot(X-Mu,(X-Mu)./Sigma,3);

% wi*1/{[(2*pi).^D/2]*Ei.^1/2} (Reynolds-Rose)
% c, const -> (T,M) and then multiply by old Y
lcoi = log(2.*pi).*(L./2)+0.5.*sum(log(sigma),1);  % c,const -> (T,M)
lcoef = repmat(log(c')-lcoi,[T,1]);

if DEBUG1 
    lcoi,lcoef,lY,pause;
end

YM = lcoef+lY;            % ( T,M ) one mixture per column
Y = lsum(YM,2);           % add mixtures 

if DEBUG 
    [size(YM) NaN size(Y)], 
end
