%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:crea la grafica ROC a partir resultados de FP y FR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
% clase=2;

for clase= 1:7
stclase=num2str(clase);
fp1=cell2mat(FPm(clase));
fn1=cell2mat(FNm(clase));

%oscilaciones por pocas medidas
for i = 1:length(fp1)-1;
    if fp1(i)<fp1(i+1);
       fp1(i)=fp1(i+1);
       
    end
    if fn1(i) > fn1(i+1);
       fn1(i) = fn1(i+1);
    end
end

[b,a] = BUTTER(2,0.1);  %0.3
fpf1 = filtfilt(b,a,fp1);
fnf1 = filtfilt(b,a,fn1);


%rectificacion
for i = 1:length(fp1);
  if fnf1(i)>100
      fnf1(i)=100;
  end
  if fnf1(i)< 0
      fnf1(i)=0;
  end

  if fpf1(i)< 0
      fpf1(i)=0;
  end
end
col=hsv(7);

acierto=100-fnf1;

plot(fpf1,acierto,'color',col(clase,:),'LineWidth',2);
hold on

%calculo area
% ar(clase)=(polyarea(fpf1/100,acierto/100));
% ar(clase)=100-(100*area(clase));
AUC(clase)=trapz(fpf1,acierto)/100;
% AUC(clase) = ppval(fnint(csape(fpf1,acierto)),max(fpf1)); 
% AT=1e5;
% AUC(clase) =AreaUnderCurve(fpf1,acierto)/100;

end

% 100-(100*AUC/AT) %porcentaje de AUC
% 100+(AUC/1000)
AUC

% random_clase=[0:100/length(fpf1):100-(1/length(fpf1))];
% random_clase=[0:99];
% plot(random_clase)

msg = 'ROC';
title([msg],'FontName','Arial','FontSize', 14)
xlabel('FP','FontName','Arial','FontSize', 14);ylabel('Verification','FontName','Arial','FontSize', 14);
legend('class1','class2','class3','class4','class5','class6','class7','FontName','Arial','FontSize', 14);
% legend('test','random','FontName','Arial','FontSize', 14);
% xlabel('FA');ylabel('Verification');
% legend('class1','class2');
% %grafica ROC
% msg = strcat('ROC',stclase);
% str = strcat('C:\Users\Aar�n\Documents\MATLAB\results7\ROC\',msg);
% print('-djpeg90', str);

