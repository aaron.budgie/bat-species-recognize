%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:%extrae la frecuencia central de las llamadas sin rudio para a�adirla al
%vector de caracter�sticas.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [fc,fmin,fmax,fini,fend,fmid,bw,st1,st2]=fc_ex_pruebasdouble(clase,num,duracion)

tvo=100;%100,30,60
tva=20;%50,30

% dir1 = 'C:\Users\Aar�n\Documents\copia tipos de llamadas\sinruido\';
% dir1= 'C:\Users\Aar�n\Documents\copia tipos de llamadas\tipos de llamadas\';
dir1= 'C:\Users\Aar�n\Documents\copia tipos de llamadas\3clasificacion\';
dir=strcat(dir1,'clase',clase,'\');
   
% file = strcat(dir,'muestra_sinruido',num,'.wav');
file = strcat(dir,'muestra_original',num,'.wav');
 

% msg = sprintf('Reading denoise file: %s',file); disp(msg);

[y,fs] = wavread(file);

[fmin,fmax]=acotar_call(y,fs,tvo,0);


file = strcat(dir,'muestra_audible',num,'.wav');

[y,fs] = wavread(file);

% [fc,fmin,fmax,fini,fend,bw,st1,st2]= histogram_fc_pruebas(y,fs,tvo,duracion);
[fc,fini,fend,fmid,bw,st1,st2]= histogram_fc_pruebas3_mod(y,fs,tva,duracion,fmin,fmax,0,clase);

%normailizacion frecuencias 1= 22050Hz (antes multiplicaba todo por 5  y bw
%por 10)
% alfa=22050;
alfa=5;
fc=fc*alfa;
fmin=fmin*alfa;
fmax=fmax*alfa;
fini=fini*alfa;
fend=fend*alfa;
fmid=fmid*alfa;
bw=bw*2*alfa;

alfa=200;
st1.media=st1.media/alfa;
st1.varianza=st1.varianza/alfa;
st1.skew=st1.skew;
st1.kurt=st1.kurt;
st1.tupac=st1.tupac*alfa;

st2.media=st2.media/alfa;
st2.varianza=st2.varianza/alfa;
st2.skew=st2.skew;
st2.kurt=st2.kurt;
st2.tupac=st2.tupac*alfa;

%calculo pitch
% x=my_vad4(y,0);
% pitch=estimadorpicth(x(ceil(length(x)/2):ceil(length(x)*3/5)),fs);
% pitch=pitch/1e4;