%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:calcula histograma (suma filas de un espectrograma y los valores de 
%frecuencia central,maxima y minima para el vector de caracteristicas y las
% curvas de forma
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [fc,fini,fend,fmid,bw,st1,st2]= histogram_fc_pruebas3_mod(x,Fs,tvo,duracion,fminr,fmaxr,pintar,class)

fi=[];
fi2=[];
loc=[];


%%% Paso 1 acotar la llamada en frecuencia

[B,F,T]= SPECTROGRAM(x,hanning(tvo),tvo/2,1024,Fs);%puntos 1024,1024*2
if pintar==1
   imagesc(T,F,(abs(B)));axis xy
end
% figure
% imagesc(T,F,10*log10(abs(B)));axis xy

sB=size(B);
aB=abs(B);
% h=sum(B(:,1:sB(2)/2)'); %sumo las filas solo hasta la mitad (por si hay dos locuciones que molestan)
% h=abs(h);
h=sum(aB');
% umbral al 80% del maximo
 umbral=0.2*max(h);
%me quedo con la parte que supera el umbral
I=find(h>umbral);

% if length(I) < 2  % si no hay muestra (evita error)
%    I=[20 54];%inventada solo para q no pare simulacion
% end
% figure
% plot(h(I))
s=size(I);
f=I(1); %el first es el primer valor
l=I(s(2));  %el last es el ultimo  la correccion +50 es experimental

%para evitar errores al sesgar
sB=size(B);
if sB(1)<l
    delta=l-f;
    l=l-50;%l-50
    f=l-delta;%l-delta
end


%el maximo de h corresponde con su maximo en frecuencia
s2=size(h);
fmin=f/s2(2);
fmax=l/s2(2);

bw=fmax-fmin;
pos_max=find(h==(max(h)));
fc=pos_max/s2(2);

% if pintar==1 %pinta una linea en fc
%    imagesc(T,F,(abs(B)));axis xy
%    hold on
%    sB=size(B);
%    sBc=sB(2);
%    fco=fc*max(F);
%    fcv=repmat(fco,sBc,1);
%    plot(T,fcv,'r','LineWidth',2);
%    stclase=num2str(class);
%    msg = strcat('Obtenci�n frecuencia m�xima energ�a clase',stclase);
%    msg2 = strcat('fcc',stclase);
%    title([msg])
%    xlabel('Tiempo(s)');ylabel('Frecuencia(Hz)');
%    str = strcat('C:\Users\Aar�n\Documents\MATLAB\figurasmemoria\fc\',msg2);
%    print('-djpeg90', str);
% end

% if pintar==1 %pinta una linea en fmin y fmax
%    figure;
%    imagesc(T,F,(abs(B)));axis xy
%    hold on
%    sB=size(B);
%    sBc=sB(2);
%    fo=fmin*max(F);
%    lo=fmax*max(F);
%    fv=repmat(fo,sBc,1);
%    lv=repmat(lo,sBc,1);
%    plot(T,fv,'r','LineWidth',2);
%    hold on
%    plot(T,lv,'r','LineWidth',2);
%    title('Acotar locucion clase 2')
%    xlabel('Tiempo(s)');ylabel('Frecuencia(Hz)');
% end


%calculo frec inicial y final (si hay varias llamadas coge el final de la
%ultima)
Bs=B(f:l,:);  %solo cojo las filas pertenecientes a la llamada%f-20
% size(Bs)
%%%Paso 2 Normalizacion
aBs=abs(Bs);

if pintar ==1
   figure;
   imagesc(T,F(f:l),aBs);axis xy
end
mx=max(max(aBs));
% size(mx)
% aBs=aBs/mx;  %lo quito porque se ve peor

%%% Paso 3 acotar Bs a la llamada en tiempo con umbral y duracion del dav
v=sum(Bs);
v=abs(v);

umbral=0.1*max(v);  %umbral para el tiempo

I=find(v>=umbral);   %la clase 8 da vector v = 0 para alguna muestra


first=I(1); %el first es el primer valor
last=first+ceil(duracion);%es por uno, es decir sin multiply%2.5


if pintar==1 %pinta una linea en fmin y fmax
   figure;
   ax=axes;
   imagesc(T,F(f:l),(abs(aBs)));axis xy
   hold on
   sB=size(aBs);
   sBc=sB(2);
   fo=first*max(T)/sBc;
   lo=last*max(T)/sBc;
   hold on
   line([fo fo], [F(f) F(l)],'color',[1 0 0],'LineWidth',2);
   line([lo lo], [F(f) F(l)],'color',[1 0 0],'LineWidth',2);
   stclase=num2str(class);
   msg = strcat('Acotar locucion clase ',stclase);
   msg2 = strcat('d',stclase);
   title([msg])
   xlabel('Tiempo(s)');ylabel('Frecuencia(Hz)');
   str = strcat('C:\Users\Aar�n\Documents\MATLAB\figurasmemoria\dur\',msg2);
%    print('-djpeg90', str);
end



loc=Bs(:,first:last); % me quedo con la llamada(corto en tiempo)
loc=abs(loc);
if pintar ==1
   figure;
%    imagesc(T(first:last),F,loc);
   imagesc(loc);axis xy;
   eje=axis;
end


%%%Paso 4 Encontrar m�ximo, si hay mas de uno el del centro
sloc=size(loc);
for i =1:sloc(2)
%    max(loc(:,i))
   fi(i)=round(mean(find(loc(:,i)==max(loc(:,i)))));  %por el m�ximo(si hay mas de un valor igual cojo el centro)
   fi2(i)= sum(loc(:,i)); %Por la suma de todas las frecuencias
   
%    round(mean(find(a==max(a))))
end
[b,a] = BUTTER(2,0.1);%%%orden4 lo cambio a 2 por problema longitud clase 7
fi = filtfilt(b,a,fi);

fi2 = filtfilt(b,a,fi2);

%%% Paso 5 obtencion de parametros a partir de la curva de fi y
%%% normalizacion
N=sloc(2);
% fini=fi(1)/length(fi);
% fend=fi(N)/length(fi);
% fm=fi(ceil(N/2));

%ahora se comprueba a la frecuencia a la que comienza y termina la llamada
%viendo la frecuencia de mas energia al comienzo de la misma
%no lo hago con la curva de fi porque pierdo los valores en frecuencia
val_max=max(Bs(:,first)); %valor maximo
fini=round(mean(find(B(:,first)==val_max))); %la posicion la busco en la matriz inicial
fini=fini/s2(2);

val_max=max(Bs(:,ceil(last))); %valor maximo
fend=round(mean(find(B(:,ceil(last))==val_max))); %la posicion la busco en la matriz inicial
fend=fend/s2(2);

mid=ceil((last+first)/2);%estaba un menos
val_max=max(Bs(:,ceil(mid))); %valor maximo
fmid=round(mean(find(B(:,ceil(mid))==val_max))); %la posicion la busco en la matriz inicial
fmid=fmid/s2(2);


% if pintar==1 %pinta una linea en fini,fend y fmid
%    figure;
%    imagesc(T,F,(abs(B)));axis xy
%    hold on
%    sB=size(B);
%    sBc=sB(2);
%    fi=fini*max(F);
%    fe=fend*max(F);
%    fm=fmid*max(F);
%    fv=repmat(fi,sBc,1);
%    lv=repmat(fe,sBc,1);
%    mv=repmat(fm,sBc,1);
%    plot(T,fv,'r','LineWidth',2);
%    hold on
%    plot(T,lv,'g','LineWidth',2);
%    hold on
%    plot(T,mv,'y','LineWidth',2);
%    fo=first*max(T)/sBc;
%    lo=last*max(T)/sBc;
%    mo=mid*max(T)/sBc;
%    hold on
%    line([fo fo], [F(1) F(end)],'color',[1 0 0],'LineWidth',2);
%    line([lo lo], [F(1) F(end)],'color',[0 1 0],'LineWidth',2);
%    line([mo mo], [F(1) F(end)],'color',[1 1 0],'LineWidth',2);
%    stclase=num2str(class);
%    msg = strcat('fini fend fmid clase',stclase);
%    msg2 = strcat('iem',stclase);
%    title([msg])
%    xlabel('Tiempo(s)');ylabel('Frecuencia(Hz)');
%    str = strcat('C:\Users\Aar�n\Documents\MATLAB\figurasmemoria\iniendmid\',msg2);
% %    print('-djpeg90', str);
% end

%normalizamos a la frecuecia real, tanto frecuencias como curvas
factor_norm=(fmaxr-fminr)/(fmax-fmin);

fini=fminr+factor_norm*(fini-fmin);
fend=fminr+factor_norm*(fend-fmin);
fmid=fminr+factor_norm*(fmid-fmin);
fc=fminr+factor_norm*(fc-fmin);

% for i = 1:length(fi)
%     fi(i)=fminr+factor_norm*(fi(i)-fmin);
%     fi2(i)=fminr+factor_norm*(fi2(i)-fmin);
% end


% sfi=size(fi)
if pintar==1
   hold on
   plot(fi,'g','LineWidth',2)
   axis(eje);
%    figure;
   plot(fi2/2,'k','LineWidth',2)
   
   stclase=num2str(class);
   msg = strcat('Curvas de forma clase',stclase);
   msg2 = strcat('cf',stclase);
   title([msg])
   xlabel('Tiempo');ylabel('Frecuencia');
   legend('forma','energia');
   str = strcat('C:\Users\Aar�n\Documents\MATLAB\figurasmemoria\forma\',msg2);
%    print('-djpeg90', str);
end

st1.media=mean(fi);
st1.varianza=std(fi);
st1.skew=skewness(fi);
st1.kurt=kurtosis(fi);
st1.tupac=(1/N)*(1/abs(max(fi)))*sum(abs(diff(fi)));

st2.media=mean(fi2);
st2.varianza=std(fi2);
st2.skew=skewness(fi2);
st2.kurt=kurtosis(fi2);
st2.tupac=(1/N)*(1/abs(max(fi2)))*sum(abs(diff(fi2)));



close all

end