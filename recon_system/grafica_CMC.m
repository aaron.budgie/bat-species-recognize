%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Autor:Aaron Henriquez Quintana
%Fecha:08/2013
%%Descripci�n:crea la grafica CMC a partir resultados de 8 iteraciones del
%%sistema
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
% clase=13;
num_clases=7;

for clase= 1:num_clases;
    
stclase=num2str(clase);
cl1=cell2mat(clasificacion(clase));
cl2=cell2mat(clasificacion2(clase));
cl3=cell2mat(clasificacion3(clase));
cl4=cell2mat(clasificacion4(clase));

s=size(cl1);

for i=1:s(2)
   
   ocl1=sort(cl1(:,i),'descend');  
   ocl2=sort(cl2(:,i),'descend');
   ocl3=sort(cl3(:,i),'descend');  
   ocl4=sort(cl4(:,i),'descend');
   
   for j=1:num_clases
      
       if ocl1(j)==(cl1(clase,i)); %compruebo posicion clase en test
          winner1(i)=j;
       end
       if ocl2(j)==(cl2(clase,i)); %compruebo posicion clase
          winner2(i)=j;
       end
       if ocl3(j)==(cl3(clase,i)); %compruebo posicion clase
          winner3(i)=j;
       end
       if ocl4(j)==(cl4(clase,i)); %compruebo posicion clase
          winner4(i)=j;
       end       
   end
   
end

winner=[winner1,winner2,winner3,winner4];

for k = 1:num_clases
    curva(k)=length(find(winner<k+1))*100/length(winner);
end

col=hsv(num_clases);
ejex=[1:num_clases];
plot(ejex,curva,'color',col(clase,:),'LineWidth',2);
grid;
hold on
end

msg = 'CMC';
title([msg])
xlabel('Rank');ylabel('Verification');
legend('class1','class2','class3','class4','class5','class6','class7');
% legend('class1','class2');

% % grafica CMC
% msg = strcat('CMC',stclase);
str = strcat('C:\Users\Aar�n\Documents\MATLAB\results7\CMC\',msg);
print('-djpeg90', str);

